<?php

namespace App\Http\Controllers;

use App\Models\Categoria;
use App\Models\Marca;
use App\Models\Producto;
use Illuminate\Http\Request;

class CategoriaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $categorias=Categoria::paginate(4);
        return view('categorias',['categorias'=>$categorias]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('categoriaCreate');
    }

    /**
     * Function global for validate Form .
     *
     *
     */
    private function validarForm(Request $request)
    {
        $request->validate(
            [ 'catNombre'=>'required|unique:categorias,catNombre|min:2|max:30' ],
            [
                'catNombre.required'=>'El campo "Nombre de la categoria" es obligatorio.',
                'catNombre.min'=>'El campo "Nombre de la categoria" debe tener al menos 2 caractéres.',
                'catNombre.max'=>'El campo "Nombre de la categoria" debe tener 30 caractéres como máximo.',
                'catNombre.unique'=>'No puede haber dos categorias con el mismo nombre.'
            ]
        );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {


            $catNombre=$request->catNombre;
            $this->validarForm($request);

            $Categoria=new Categoria;
            $Categoria->catNombre = $catNombre = $request->catNombre;
            $Categoria->save();

            return redirect('/categorias')
                ->with(['mensaje' => 'Categoria: '.$catNombre.' agregada correctamente']);

        }
        catch (\Throwable $th) {

            return redirect('/marcas')->with(['mensaje' => 'No se pudo modificar la región']);
        }


    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Categoria  $categoria
     * @return \Illuminate\Http\Response
     */
    public function show(Categoria $categoria)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $Categoria=Categoria::find($id);
        return view('categoriaEdit',['Categoria'=>$Categoria]);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $this->validarForm($request);

        try {

            $Categoria = Categoria::find( $request->idCategoria );

            $Categoria->catNombre = $catNombre = $request->catNombre;

            $Categoria->save();

            return redirect('/categorias')
                ->with(['mensaje' => 'Categoria: '.$catNombre.' modificada correctamente']);
        } catch (\Throwable $th)
        {
            //throw $th;
            return redirect('/categorias')->with(['mensaje' => 'No se pudo modificar la categoria.']);
        }
    }
    /**
     * Remove the specified resource from storage.
     *
     * Method
     */

    private function checkProducto($idCategoria)
    {
        $check = Producto::where('idCategoria', $idCategoria)->count();
        return $check;
    }

    public function confirm($id)
    {
        $Categoria=Categoria::find($id);

        if ( $this->checkProducto($id) == 0 ){
            return view('categoriaDelete', [ 'Categoria'=>$Categoria ]);
        }

        return redirect('/categorias')
            ->with(
                [
                    'clase'=>'warning',
                    'mensaje'=>'No se puede eliminar la categoria '.$Categoria->catNombre.' ya que tiene productos relacionados.'
                ]);
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        try {

            $Categoria = Categoria::destroy($request->idCategoria);

            return redirect('/categorias')
                ->with(['mensaje' => 'Categoria: '.$request->catNombre.' eliminada correctamente']);
        } catch (\Throwable $th)
        {

            return redirect('/categorias')->with(['mensaje' => 'No se pudo eliminar la categoria.']);
        }
    }
}
