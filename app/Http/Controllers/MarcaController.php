<?php

namespace App\Http\Controllers;

use App\Models\Marca;
use App\Models\Producto;
use Illuminate\Http\Request;

class MarcaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $marcas = Marca::paginate(4);
        return view('marcas', ['marcas'=>$marcas]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('marcaCreate');
    }

    /**
     * Function global for validate Form .
     *
     *
     */
    private function validarForm(Request $request)
    {
        $request->validate(
            [ 'mkNombre'=>'required|unique:marcas,mkNombre|min:2|max:30' ],
            [
                'mkNombre.required'=>'El campo "Nombre de la marca" es obligatorio.',
                'mkNombre.min'=>'El campo "Nombre de la marca" debe tener al menos 2 caractéres.',
                'mkNombre.max'=>'El campo "Nombre de la marca" debe tener 30 caractéres como máximo.',
                'mkNombre.unique'=>'No puede haber dos marcas con el mismo nombre.'
            ]
        );
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {


            $mkNombre=$request->mkNombre;
            $this->validarForm($request);

            $Marca=new Marca;
            $Marca->mkNombre = $mkNombre = $request->mkNombre;
            $Marca->save();

            return redirect('/marcas')
                ->with(['mensaje' => 'Marca: '.$mkNombre.' agregada correctamente']);

        }
        catch (\Throwable $th) {

            return redirect('/marcas')->with(['mensaje' => 'No se pudo modificar la región']);
        }

    }


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $Marca = Marca::find($id);
        return  view('marcaEdit', [ 'Marca'=>$Marca ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $this->validarForm($request);
        try {

            $Marca = Marca::find( $request->idMarca );

            $Marca->mkNombre = $mkNombre = $request->mkNombre;

            $Marca->save();

            return redirect('/marcas')
                ->with(['mensaje' => 'Marca: '.$mkNombre.' modificada correctamente']);
        } catch (\Throwable $th)
        {
            //throw $th;
            return redirect('/marcas')->with(['mensaje' => 'No se pudo modificar la marca.']);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * Method
     */

    private function checkProducto($idMarca)
    {
        $check = Producto::where('idMarca', $idMarca)->count();
        return $check;
    }

    public function confirm($id)
    {
    $Marca=Marca::find($id);

        if ( $this->checkProducto($id) == 0 ){
            return view('marcaDelete', [ 'Marca'=>$Marca ]);
        }

        return redirect('/marcas')
            ->with(
                [
                    'clase'=>'warning',
                    'mensaje'=>'No se puede eliminar la marca '.$Marca->mkNombre.' ya que tiene productos relacionados.'
                ]);
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        try {

            $Marca = Marca::destroy($request->idMarca);

            return redirect('/marcas')
                ->with(['mensaje' => 'Marca: '.$request->mkNombre.' eliminada correctamente']);
        } catch (\Throwable $th)
        {

            return redirect('/marcas')->with(['mensaje' => 'No se pudo eliminar la marca.']);
        }
    }






}
